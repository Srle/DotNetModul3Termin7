$(document).ready(function(){

	// podaci od interesa
	var host = "http://localhost:";
	var port = "60067/";
	var reziseriEndpoint = "api/reziser/";
	var filmoviEndpoint = "api/film/";	
	var formAction = "Create";
	var editingId;
		
	//REŽISER:

	// prikaz režisera
	$("#btnReziseri").click(function(){
		// ucitavanje režisera
		var requestUrl = host + port + reziseriEndpoint;
		console.log("URL zahteva: " + requestUrl);
		$.getJSON(requestUrl, setReziseri);
	});

	// metoda za postavljanje rezisera u tabelu
	function setReziseri(data, status){
		console.log("Status: " + status);

		var $container = $("#data");
		$container.empty(); 
		
			// sakrivanje forme filma
			$("#formDiv2").css("display","none");
			$container.append(div);
		
		if (status == "success")
		{
			console.log(data);
			// ispis naslova
			var div = $("<div></div>");
			var h1 = $("<h1>Režiseri</h1>");
			div.append(h1);
			// ispis tabele režisera
			var table = $("<table></table>");
			var header = $("<tr><td>Id</td><td>Ime</td><td>Prezime</td><td>Starost</td><td>Delete</td><td>Edit</td></tr>");
			table.append(header);
			for (i=0; i<data.length; i++)
			{
				// prikazujemo novi red u tabeli
				var row = "<tr>";
				// prikaz podataka
				var displayData = "<td>" + data[i].Id + "</td><td>" + data[i].Ime + "</td><td>" + data[i].Prezime + "</td><td>" + data[i].Starost + "</td>";
				// prikaz dugmadi za izmenu i brisanje
				var stringId = data[i].Id.toString();
				var displayDelete = "<td><button id=btnDeleteReziser name=" + stringId + ">Delete</button></td>";
				var displayEdit = "<td><button id=btnEditReziser name=" + stringId + ">Edit</button></td>";
				row += displayData + displayDelete + displayEdit + "</tr>";  
				table.append(row);
			}
			
			div.append(table);

			// prikaz forme
			$("#formDiv1").css("display","block");

			// ispis novog sadrzaja
			$container.append(div);
		}
		else 
		{
			var div = $("<div></div>");
			var h1 = $("<h1>Greška prilikom preuzimanja Režisera!</h1>");
			div.append(h1);
			$container.append(div);
		}
	};

	// dodavanje novog režisera
	$("#formReziser").submit(function(e){
		// sprecavanje default akcije forme
		e.preventDefault();

		var reziserIme = $("#imeReziser").val();
		var reziserPrezime = $("#prezimeReziser").val();
		var reziserStarost = $("#starostReziser").val();
		var httpAction;
		var sendData;
		var url;

		// u zavisnosti od akcije pripremam objekat režiser
		if (formAction === "Create") {
			httpAction = "POST";
			url = host + port + reziseriEndpoint;
			sendData = {
				"Ime": reziserIme,
				"Prezime": reziserPrezime,
				"Starost": reziserStarost
			};
		}
		else {
			httpAction = "PUT";
			url = host + port + reziseriEndpoint + editingId.toString();
			sendData = {
				"Id": editingId,
				"Ime": reziserIme,
				"Prezime": reziserPrezime,
				"Starost": reziserStarost
			};
		}
		

		console.log("Objekat za slanje:");
		console.log(sendData);

		$.ajax({
			url: url,
			type: httpAction,
			data: sendData
		})
		.done(function(data, status){
			formAction = "Create";
			refreshTable();
		})
		.fail(function(data, status){
			alert("Desila se greska!");
		})

	});
	
	// pripremanje dogadjaja za brisanje režisera
	$("body").on("click", "#btnDeleteReziser", deleteReziser);

	// priprema dogadjaja za izmenu režisera
	$("body").on("click", "#btnEditReziser", editReziser);

	// brisanje režisera
	function deleteReziser() {
		// izvlacimo {id}
		var deleteID = this.name;
		// saljemo zahtev 
		$.ajax({
			url: host + port + reziseriEndpoint + deleteID.toString(),
			type: "DELETE",
		})
		.done(function(data, status){
			refreshTable();
		})
		.fail(function(data, status){
			alert("Desila se greska!");
		});

	};

	// izmena režisera
	function editReziser(){
		// izvlacimo id
		var editId = this.name;
		// saljemo zahtev da dobavimo tog autora
		$.ajax({
			url: host + port + reziseriEndpoint + editId.toString(),
			type: "GET",
		})
		.done(function(data, status){
			$("#imeReziser").val(data.Ime);
			$("#prezimeReziser").val(data.Prezime);		
			$("#starostReziser").val(data.Starost);			
			editingId = data.Id;
			formAction = "Update";
		})
		.fail(function(data, status){
			formAction = "Create";
			alert("Desila se greska!");
		});

	};

	// osvezi prikaz tabele režisera
	function refreshTable() {
		// cistim formu
		$("#imeReziser").val('');
		$("#prezimeReziser").val('');		
		$("#starostReziser").val('');		
		// osvezavam
		$("#btnReziseri").trigger("click");
	};

		//FILM:

	// prikaz filmovi
	$("#btnFilmovi").click(function(){
		// ucitavanje filma
		var requestUrl = host + port + filmoviEndpoint;
		console.log("URL zahteva: " + requestUrl);
		$.getJSON(requestUrl, setFilmovi);
	});

	// metoda za postavljanje filma u tabelu
	function setFilmovi(data, status){
		console.log("Status: " + status);

		var $container = $("#data");
		$container.empty(); 
		
			// sakrivanje forme režisera
			$("#formDiv1").css("display","none");
			$container.append(div);
		
		if (status == "success")
		{
			console.log(data);
			// ispis naslova
			var div = $("<div></div>");
			var h1 = $("<h1>Filmovi</h1>");
			div.append(h1);
			// ispis tabele filmova
			var table = $("<table></table>");
			var header = $("<tr><td>Id</td><td>Ime</td><td>Žanr</td><td>Godina</td><td>Ime i prezime režisera</td><td>Delete</td><td>Edit</td></tr>");
			table.append(header);
			for (i=0; i<data.length; i++)
			{
				// prikazujemo novi red u tabeli
				var row = "<tr>";
				// prikaz podataka
				var displayData = "<td>" + data[i].Id + "</td><td>" + data[i].Ime + "</td><td>" + data[i].Zanr + 
				"</td><td>" + data[i].Godina + "</td><td>" + data[i].Reziser.Ime + " " + data[i].Reziser.Prezime + "</td>";
				// prikaz dugmadi za izmenu i brisanje
				var stringId = data[i].Id.toString();
				var displayDelete = "<td><button id=btnDeleteFilm name=" + stringId + ">Delete</button></td>";
				var displayEdit = "<td><button id=btnEditFilm name=" + stringId + ">Edit</button></td>";
				row += displayData + displayDelete + displayEdit + "</tr>";  
				table.append(row);
			}
			
			div.append(table);
			
			//////////////////////////////////////////////////////////////////////////////////
			
			// ucitavanje rezisera u select
			var requestUrl1 = host + port + reziseriEndpoint;
			console.log("URL zahteva: " + requestUrl1);
			$.getJSON(requestUrl1, MeniReziseri);
			
			// pravljenje padajućeg menija režisera ispod tabele filmova
			function MeniReziseri(data1, status)
			{
				console.log("Status: " + status);
			
				var meni = $("#selectReziser");
				meni.empty();
			
				if (status == "success")
				{
					//pravljenje stavki menija režisera
					var stavke ="";
					for (i=0; i<data1.length; i++)
					{
						stavke += "<option value="+data1[i].Id +">" + data1[i].Ime + " " + data1[i].Prezime + "</option>";
					}
					meni.append(stavke);
				}
				else 
				{
					var div = $("<div></div>");
					var h1 = $("<h1>Greška prilikom preuzimanja Režisera za meni!</h1>");
					div.append(h1);
					$container.append(div);
				}
		
			}
			////////////////////////////////////////////////////////////////
			
			// prikaz forme
			$("#formDiv2").css("display","block");

			// ispis novog sadrzaja
			$container.append(div);
		}
		else 
		{
			var div = $("<div></div>");
			var h1 = $("<h1>Greška prilikom preuzimanja Filma!</h1>");
			div.append(h1);
			$container.append(div);
		}
	};


	// dodavanje novog režisera
	$("#formFilm").submit(function(e){
		// sprecavanje default akcije forme
		e.preventDefault();

		var filmIme = $("#imeFilm").val();
		var filmZanr = $("#zanrFilm").val();
		var filmGodina = $("#godinaFilm").val();
		var filmReziserId = $("#selectReziser").val();
		var httpAction;
		var sendData;
		var url;

		// u zavisnosti od akcije pripremam objekat film
		if (formAction === "Create") {
			httpAction = "POST";
			url = host + port + filmoviEndpoint;
			sendData = {
				"Ime": filmIme,
				"Zanr": filmZanr,
				"Godina": filmGodina,
				"ReziserId": filmReziserId
			};
		}
		else {
			httpAction = "PUT";
			url = host + port + filmoviEndpoint + editingId.toString();
			sendData = {
				"Id": editingId,
				"Ime": filmIme,
				"Zanr": filmZanr,
				"Godina": filmGodina,
				"ReziserId": filmReziserId
			};
		}
		
		console.log("Objekat za slanje:");
		console.log(sendData);

		$.ajax({
			url: url,
			type: httpAction,
			data: sendData
		})
		.done(function(data, status){
			formAction = "Create";
			refreshTableFilm();
		})
		.fail(function(data, status){
			alert("Desila se greska!");
		})

	});

	// pripremanje dogadjaja za brisanje filma
	$("body").on("click", "#btnDeleteFilm", deleteFilm);

	// priprema dogadjaja za izmenu filma
	$("body").on("click", "#btnEditFilm", editFilm);

	// brisanje filma
	function deleteFilm() {
		// izvlacimo {id}
		var deleteID = this.name;
		// saljemo zahtev 
		$.ajax({
			url: host + port + filmoviEndpoint + deleteID.toString(),
			type: "DELETE",
		})
		.done(function(data, status){
			refreshTableFilm();
		})
		.fail(function(data, status){
			alert("Desila se greska!");
		});

	};

	// izmena filma
	function editFilm(){
		// izvlacimo id
		var editId = this.name;
		// saljemo zahtev da dobavimo taj film
		$.ajax({
			url: host + port + filmoviEndpoint + editId.toString(),
			type: "GET",
		})
		.done(function(data, status){
			$("#imeFilm").val(data.Ime);
			$("#zanrFilm").val(data.Zanr);		
			$("#godinaFilm").val(data.Godina);			
			$("#selectReziser").val(data.ReziserId);					
			editingId = data.Id;
			formAction = "Update";
		})
		.fail(function(data, status){
			formAction = "Create";
			alert("Desila se greska!");
		});

	};

	// osvezi prikaz tabele režisera
	function refreshTableFilm() {
		// cistim formu
		$("#imeFilm").val('');
		$("#zanrFilm").val('');		
		$("#godinaFilm").val('');		
		// osvezavam
		$("#btnFilmovi").trigger("click");
	};	
	
});



