﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace RadSaFilmovima.Models
{
    public class RadSaFilmovimaContext : DbContext
    {
        public RadSaFilmovimaContext() : base("name=RadSaFilmovimaContext") { }

        public DbSet<Film> Filmovi { get; set; }
        public DbSet<Reziser> Reziseri { get; set; }
    }
}