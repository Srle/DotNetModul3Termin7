namespace RadSaFilmovima.Migrations
{
    using RadSaFilmovima.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<RadSaFilmovima.Models.RadSaFilmovimaContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(RadSaFilmovima.Models.RadSaFilmovimaContext context)
        {
            context.Reziseri.AddOrUpdate(x => x.Id,
                new Reziser()
                { Id = 1, Ime = "Steven", Prezime = "Spielberg", Starost = 71 },
                new Reziser()
                { Id = 2, Ime = "Nikita", Prezime = "Mihalkov", Starost = 72 },
                new Reziser()
                { Id = 3, Ime = "Emir", Prezime = "Kusturica", Starost = 63 }
                );

            context.Filmovi.AddOrUpdate(x => x.Id,
                new Film()
                { Id = 1, Ime = "Saving private Ryan", Zanr = "ratni", Godina = 1998, ReziserId = 1 },
                new Film()
                { Id = 2, Ime = "Varljivo sunce", Zanr = "drama", Godina = 1994, ReziserId = 2 },
                new Film()
                { Id = 3, Ime = "Otac na slu�benom putu", Zanr = "drama", Godina = 1985, ReziserId = 3 },
                new Film()
                { Id = 4, Ime = "Underground", Zanr = "komedija", Godina = 1995, ReziserId = 3 }
                );
        }
    }
}
