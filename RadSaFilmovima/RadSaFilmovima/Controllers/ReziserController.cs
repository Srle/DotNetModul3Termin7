﻿using RadSaFilmovima.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace RadSaFilmovima.Controllers
{
    public class ReziserController : ApiController
    {
        private RadSaFilmovimaContext db = new RadSaFilmovimaContext();

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }

            base.Dispose(disposing);
        }

        //GET api/reziser
        public IQueryable<Reziser> GetReziser()
        {
            return db.Reziseri;
        }


        //GET api/reziser/1
        [ResponseType(typeof(Reziser))]
        public IHttpActionResult GetReziser(int id)
        {
            Reziser reziser = db.Reziseri.Find(id);
            if (reziser == null)
            {
                return NotFound();
            }

            return Ok(reziser);
        }

        //POST api/reziser
        [ResponseType(typeof(Reziser))]
        public IHttpActionResult PostAuthor(Reziser reziser)
        {
            if (!ModelState.IsValid)

            {
                return BadRequest(ModelState);
            }
            db.Reziseri.Add(reziser);
            db.SaveChanges();
            return CreatedAtRoute("DefaultApi", new { id = reziser.Id }, reziser);

        }

        private bool ReziserExists(int id)
        {
            return db.Reziseri.Count(e => e.Id == id) > 0;
        }

        // PUT api/reziser/1
        [ResponseType(typeof(Reziser))]
        public IHttpActionResult PutReziser(int id, Reziser reziser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != reziser.Id)
            {
                return BadRequest();
            }

            db.Entry(reziser).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ReziserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(reziser);
        }


        // DELETE api/reziser/1
        [ResponseType(typeof(void))]
        public IHttpActionResult DeleteReziser(int id)
        {
            Reziser reziser = db.Reziseri.Find(id);
            if (reziser == null)
            {
                return NotFound();
            }

            db.Reziseri.Remove(reziser);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }
    }
}
