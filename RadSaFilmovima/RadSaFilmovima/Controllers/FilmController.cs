﻿using RadSaFilmovima.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace RadSaFilmovima.Controllers
{
    public class FilmController : ApiController
    {
        private RadSaFilmovimaContext db = new RadSaFilmovimaContext();

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }

            base.Dispose(disposing);
        }

        //GET api/film
        public IQueryable<Film> GetFilms()
        {
            return db.Filmovi.Include(r => r.Reziser);
        }




        //GET api/film/1
        [ResponseType(typeof(Film))]
        public IHttpActionResult GetFilm(int id)
        {
            Film film = db.Filmovi.Find(id);
            if (film == null)
            {
                return NotFound();
            }

            return Ok(film);
        }

        //POST api/film
        [ResponseType(typeof(Film))]
        public IHttpActionResult PostAuthor(Film film)
        {
            if (!ModelState.IsValid)

            {
                return BadRequest(ModelState);
            }
            db.Filmovi.Add(film);
            db.SaveChanges();
            return CreatedAtRoute("DefaultApi", new { id = film.Id }, film);

        }

        private bool FilmExists(int id)
        {
            return db.Filmovi.Count(e => e.Id == id) > 0;
        }

        // PUT api/film/1
        [ResponseType(typeof(Film))]
        public IHttpActionResult PutBook(int id, Film film)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != film.Id)
            {
                return BadRequest();
            }

            db.Entry(film).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FilmExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(film);
        }

        // DELETE api/film/1
        [ResponseType(typeof(void))]
        public IHttpActionResult DeleteFilm(int id)
        {
            Film film = db.Filmovi.Find(id);
            if (film == null)
            {
                return NotFound();
            }

            db.Filmovi.Remove(film);
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }



    }
}
